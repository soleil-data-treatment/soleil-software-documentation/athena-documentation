# Aide et ressources de Athena pour Synchrotron SOLEIL

[<img src="http://bruceravel.github.io/demeter/documents/Athena/_static/pallas_athene_thumb.png" width="250"/>](http://bruceravel.github.io/demeter/documents/Athena/index.html)

## Résumé

- Analyser les résultats d'absorption X (XANES)
- Open source

## Sources

- Code source: Je ne sais pas
- Documentation officielle:  http://bruceravel.github.io/demeter/documents/Athena/index.html

## Navigation rapide

| Tutoriaux |
| - |
| [Tutoriel d'installation officiel](http://bruceravel.github.io/demeter/documents/Athena/forward.html) |
| [Tutoriaux officiels](http://bruceravel.github.io/demeter/documents/Athena/intro.html) |

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: Facile (tout se passe bien),  Sauf sur Mac où l'installation se passe très mal et aboutit à une version très instable après avoir corrigé de nombreuses erreurs

## Format de données

- en entrée: Fichier texte (.txt)
- en sortie: Fichier projet d'Athena (.prj),  fichier texte (.txt) et fichier image (.png,  .gif etc)
- sur la Ruche locale
